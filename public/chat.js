var chatPass = localStorage.getItem("chatPassStorage");

console.log(chatPass);
function init(){
    var chatroomTitle = document.getElementById("chatroomTitle");
    chatroomTitle.innerHTML = "Chat Room " + chatPass;
    var user_email = '';
    firebase.auth().onAuthStateChanged(function(user) {
        var menu = document.getElementById('dynamicMenu');
        // Check user login
        if (user) {
            user_email = user.email;
            menu.innerHTML = "<a class='dropdown-item' id='logout-btn'>Log Out</a><div class='dropdown-divider'></div><a class='dropdown-item'>" + user.email +"</a>";
            var logoutbtn = document.getElementById('logout-btn');
            logoutbtn.addEventListener("click", function() {
                firebase.auth().signOut().then(function() {
                    alert("User sign out success!");
                    window.location.href = "index.html";
                }).catch(function(error) {
                    alert("User sign out failed!");
                })
            }, false);
        } else {
            menu.innerHTML = "<a class='dropdown-item' href='./index.html'>Login</a>";
            document.getElementById('post_list').innerHTML = "";
        }
    });

    //google notification
    function createNotify() {
        if (!("Notification" in window)) { 
          console.log("This browser does not support notification");
        } else if (Notification.permission === "granted") { 
          var notification = new Notification(
            "Thanks for granting the notification permission.", notifyConfig
          );
        } else if (Notification.permission !== "denied") {
          Notification.requestPermission(function(permission) {
            if (permission === "granted") {
              var notification = new Notification("Hi there!", notifyConfig);
            }
          });
        }
      }
    createNotify();

    var notifyConfig = {
        //body: 
        icon: "./doggo.jpg"
    }

    
    

    post_btn = document.getElementById('post_btn');
    post_txt = document.getElementById('comment');
    
    
    post_btn.addEventListener('click', function() {
        var Today = new Date();
        var Today_Date = " ";
        var Hour = " ";
        var Minute = " ";
        if(Today.getHours() < 10){
            Hour = "0" + Today.getHours();
        }else{
            Hour = Today.getHours();
        }
        if(Today.getMinutes() < 10){
            Minute = "0" + Today.getMinutes();
        }else{
            Minute = Today.getMinutes();
        }

        Today_Date = Today.getFullYear() + "/" + (Today.getMonth()+1) + "/" + Today.getDate() + ", " + Hour + ":" + Minute;
        console.log(Today_Date);
        if (post_txt.value != "") {
            var Ref = firebase.database().ref(chatPass+"_list/chat");
            var data = {
                data: post_txt.value,
                email: user_email,
                date: Today_Date,
                type: 0,
                url: " "

            };
            Ref.push(data);
            
            post_txt.value = "";
        }
    });

    img_btn = document.getElementById('img_btn');

    img_btn.addEventListener('change', function(){
        var Today = new Date();
        var Today_Date = " ";
        var Hour = " ";
        var Minute = " ";
        if(Today.getHours() < 10){
            Hour = "0" + Today.getHours();
        }else{
            Hour = Today.getHours();
        }
        if(Today.getMinutes() < 10){
            Minute = "0" + Today.getMinutes();
        }else{
            Minute = Today.getMinutes();
        }

        Today_Date = Today.getFullYear() + "/" + (Today.getMonth()+1) + "/" + Today.getDate() + ", " + Hour + ":" + Minute;
        var file = this.files[0];
        console.log(file);
        var storageRef = firebase.storage().ref(file.name);
        storageRef.put(file).then(function(){
            storageRef.getDownloadURL().then(function(url){
                console.log(url);
                var Ref = firebase.database().ref(chatPass+"_list/chat");
                var data = {
                    data: '',
                    email: user_email,
                    // Type 1 for image
                    date: Today_Date,
                    type: 1,
                    url: url
                };
                Ref.push(data);
            });
        });
    });

   
    
    // The html code for post
    var str_before_mainuser_username = 
    "<div class=' bg-dark rounded box-shadow text-white'><div class='media pt-3 text-right' ><p class='media-body pb-1 mb-0 small lh-125 '><strong class='d-block text-gray-dark'>";
    var str_before_otheruser_username = 
    "<div class=' bg-dark rounded box-shadow text-white'><div class='media pt-3 text-left' ><p class='media-body pb-1 mb-0 small lh-125 '><strong class='d-block text-gray-dark'>";
    var str_after_content = "</p></div></div>\n";

    var str_before_img = "<img class='img pt-2' style='height: 300px;' src='";
    var str_after_img = "'>";


    var postsRef = firebase.database().ref(chatPass+"_list/chat");
    // List for store posts html
    var total_post = [];
    // Counter for checking history post update complete
    var first_count = 0;
    // Counter for checking when to update new post
    var second_count = 0;

    postsRef.once('value')
        .then(function(snapshot) {
            snapshot.forEach(function(childshot) {
                var childData = childshot.val();
                var dataWithoutHTML = String(childData.data).replace(/<[^>]*>/g, '');
                if(childData.type === 0){
                    if(childData.email == user_email){
                        total_post[total_post.length] = str_before_mainuser_username + childData.email + "</strong>" + dataWithoutHTML + str_after_content + "<p class='text-white text-right  media-body pb-1 mb-0 small lh-125 font-italic'>" + childData.date + "</p>";
                    }else{
                        total_post[total_post.length] = str_before_otheruser_username + childData.email + "</strong>" + dataWithoutHTML + str_after_content + "<p class='text-white text-left  media-body pb-1 mb-0 small lh-125 font-italic'>" + childData.date + "</p>";
                    }
                }else{
                    if(childData.email == user_email){
                        total_post[total_post.length] = str_before_mainuser_username + childData.email + "</strong>" + str_before_img + childData.url + str_after_img + str_after_content + "<p class='text-white text-right  media-body pb-1 mb-0 small lh-125 font-italic'>" + childData.date + "</p>";
                    }else{
                        total_post[total_post.length] = str_before_otheruser_username + childData.email + "</strong>" + str_before_img + childData.url + str_after_img + str_after_content + "<p class='text-white text-left  media-body pb-1 mb-0 small lh-125 font-italic'>" + childData.date + "</p>";
                    }
                }
                var notification = new Notification(childData.email + ": " + dataWithoutHTML, notifyConfig);
                first_count += 1

            });
            
            document.getElementById('post_list').innerHTML = total_post.join('');

            postsRef.on('child_added', function(data) {
                second_count += 1;
                if (second_count > first_count) {
                    var childData = data.val();
                    var dataWithoutHTML = String(childData.data).replace(/<[^>]*>/g, '');
                    if(childData.type === 0){
                        if(childData.email == user_email){
                            total_post[total_post.length] = str_before_mainuser_username + childData.email + "</strong>" + dataWithoutHTML + str_after_content + "<p class='text-white text-right  media-body pb-1 mb-0 small lh-125 font-italic'>" + childData.date + "</p>";
                        }else{
                            total_post[total_post.length] = str_before_otheruser_username + childData.email + "</strong>" + dataWithoutHTML + str_after_content + "<p class='text-white text-left  media-body pb-1 mb-0 small lh-125 font-italic'>" + childData.date + "</p>";
                        }
                    }else{
                        if(childData.email == user_email){
                            total_post[total_post.length] = str_before_mainuser_username + childData.email + "</strong>" + str_before_img + childData.url + str_after_img + str_after_content + "<p class='text-white text-right  media-body pb-1 mb-0 small lh-125 font-italic'>" + childData.date + "</p>";
                        }else{
                            total_post[total_post.length] = str_before_otheruser_username + childData.email + "</strong>" + str_before_img + childData.url + str_after_img + str_after_content + "<p class='text-white text-left  media-body pb-1 mb-0 small lh-125 font-italic'>" + childData.date + "</p>";
                        }
                    }
                    var notification = new Notification(childData.email + ": " + dataWithoutHTML, notifyConfig);
                    document.getElementById('post_list').innerHTML = total_post.join('');
                }
                
            });


        })
        .catch(e => console.log(e.message));
};

window.onload = function(){
    init();
};


