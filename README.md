# Software Studio 2021 Spring Midterm Project

## Topic
* Project Name : midtermproject107062275

## Basic Components
|Component|Score|Y/N|
|:-:|:-:|:-:|
|Membership Mechanism|15%|Y|
|Firebase Page|5%|Y|
|Database|15%|Y|
|RWD|15%|Y|
|Topic Key Function|20%|Y|

## Advanced Components
|Component|Score|Y/N|
|:-:|:-:|:-:|
|Third-Party Sign In|2.5%|Y|
|Chrome Notification|5%|Y|
|Use CSS Animation|2.5%|Y|
|Security Report|5%|Ｙ|

# 作品網址:
https://midtermproject107062275-f0129.web.app

## Website Detail Description
使用者首先在登入頁面進行註冊或是登入的動作，在這部分也可以以Google Account 進行登入。
成功登入後，使用者可以在首頁選擇要創建一個新的聊天室，或是輸入 Chat Room Password 以進入該 chat room 。
若使用者選擇要創建一個新的聊天室，則系統會提示使用者創建的聊天室名稱（等同於該聊天室的 Chat Room Password），接著進入新的聊天室。
在聊天室上方的 navbar 除了該聊天室的名稱外，另外有登出的選項以及返回首頁的選項。若選擇登出，則會將使用者登出並返回登入頁面。
在聊天室頁面中，使用者可以看到先前傳送過的訊息，也可以輸入新訊息。該使用者傳送出的訊息會排列在畫面左方，而非該使用者傳送出的訊息則會排列在畫面右方。所有的訊息都會顯示出輸入者的 email 以及傳送訊息的日期與時間。另外，使用者也可以透過聊天室傳送圖片。 
每次有使用者傳送新訊息時，皆會傳送通知給其他相同聊天室的使用者。通知內容包含該新訊息的傳送者以及訊息內容。
聊天室並不會消失，就算登出帳戶或離開聊天室，只要輸入 Chat Room Password ，一樣能進入相同的聊天室並看到過去傳送過的訊息及圖片。




# Components Description : 
*Basic Components*
1. Membership Mechanism : 利用 Firebase 的 Authentication 功能進行實作，使用者可以輸入信箱及密碼來註冊，其後以其登入。 Firebase 在其中會紀錄註冊信息並在登入時做對照的動作。 
2. Firebase Page : 成功使用 Firebase deploy 將網站 deploy 成功。
3. Database : 透過 Firebase 提供的 Real time database 對資訊進行read/write。
4. RWD : 這部分我是透過 Bootstrap 來達成，網頁內容會隨著頁面大小的變動去做調整。
5. Topic Key Function : 我使每一次create一個新的chat room 的時候會產生一組亂碼作為房間名，其後所有與此房間相關的資料（訊息、圖片等）皆會在 Firebase 中儲存在以此亂碼命名的list 之下，而想加入此房間的使用者只需輸入此亂碼便可進入房間。如此，便達到題目要求的private chat room的功能。

*Advanced Components*
1. Third-Party Sign In : 利用 Firebase 的 Authentication 功能進行實作，使用者可以透過 Google Account 進行註冊及登入。 
2. Chrome Notification : 如果使用者給予傳送通知的權限，即可接收到新訊息的通知，通知內容包括新訊息的傳送者 email 以及訊息內容。
4. Use CSS Animation : 在首頁的部分，我使用CSS Animation 使標題具有大小變動的效果。
6. Security Report : 在這部分我使用的方法是使用 replace() 這個 function ，將 HTML 相關的 code 剔除掉。當使用者傳遞了任何夾帶 HTML code 的訊息時，都會以最原本的文字形式呈現。 

# Other Functions Description : 
1. Upload Image: 透過Firebase的storage，提供使用者上傳圖片的功能，且能夠在聊天室顯示上傳的圖片。
2. Date Record : 紀錄使用者輸入訊息的日期及時間並以斜體顯示在訊息下方，方便使用者使用。



