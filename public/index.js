function initAccount(){
    var user = firebase.auth().currentUser;
    var txtEmail = document.getElementById('inputEmail');
    var txtPassword = document.getElementById('inputPassword');
    var btnLogin = document.getElementById('btnLogin');
    var btnSignUp = document.getElementById('btnSignUp');
    var btnGoogle= document.getElementById('btnGoogle');  

    btnLogin.addEventListener('click', function() {
        firebase.auth().signInWithEmailAndPassword(txtEmail.value, txtPassword.value).then(function(result) {
            console.log("login success");
            window.location.href = "main.html";
        }).catch(function(error) {
            txtEmail.value = "";
            txtPassword.value = "";
            console.log(error);
        });

    });
    btnSignUp.addEventListener('click', function() {
        console.log(txtEmail.value)
        console.log(txtPassword.value)
        firebase.auth().createUserWithEmailAndPassword(txtEmail.value, txtPassword.value).then(function(result) {
            txtEmail.value = "";
            txtPassword.value = "";
            console.log("signup success");
            window.location.href = "main.html";
        }).catch(function(error) {
            txtEmail.value = "";
            txtPassword.value = "";
            console.log(error.message);
        });
    });

    btnGoogle.addEventListener('click', function() {
        var provider = new firebase.auth.GoogleAuthProvider();
        firebase.auth().signInWithPopup(provider).then(function(result) {
            console.log("google signup success");
            window.location.href = "main.html";
        }).catch(function(error) {
            console.log(error.message);
        });
    });

};


window.onload = function(){
    initAccount();
};