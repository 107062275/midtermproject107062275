var chatPass;
var user_email = '';
function init(){
    firebase.auth().onAuthStateChanged(function(user) {
        var menu = document.getElementById('dynamicMenu');
            // Check user login
            if (user) {
                user_email = user.email;
                menu.innerHTML = "<a class='dropdown-item' id='logout-btn'>Log Out</a><div class='dropdown-divider'></div><a class='dropdown-item'>" + user.email +"</a>";
                var logoutbtn = document.getElementById('logout-btn');
                logoutbtn.addEventListener("click", function() {
                    firebase.auth().signOut().then(function() {
                        alert("User sign out success!");
                        window.location.href = "index.html";
                    }).catch(function(error) {
                        alert("User sign out failed!");
                    })
                }, false);
            } else {
                menu.innerHTML = "<a class='dropdown-item' href='./index.html'>Login</a>";
            }
    });

    //Create new chatroom with new chat pass
    btnCreateChat.addEventListener('click', function() {
            chatPass = Math.random().toString(36).substring(7);
            localStorage.setItem("chatPassStorage", chatPass);  
            //add the user to the chatroom's member list
            /*
            var Ref = firebase.database().ref(chatPass+"_list/member");
            console.log(user.email);
            var data = {
                email: user_email
            };
            Ref.push(data);*/
            alert("New Chat Room Create!\nYour Chat Room Password is " + chatPass);
            window.location.href = "chat.html";
    });

    //Enter the chatroom with existing ChatPass
    btnEnterChat.addEventListener('click', function() {
        chatPass = document.getElementById('inputChatPass').value;
        if(chatPass != ""){
            localStorage.setItem("chatPassStorage", chatPass); 
            //add the user to thr chatroom's member list
            /*
            var Ref = firebase.database().ref(chatPass+"_list/member");
            var data = {
                email: user_email
            };
            Ref.push(data);
            */
            alert("You are going to enter the chat room with password " + chatPass); 
            window.location.href = "chat.html";
        }else{
            alert("Please Enter the Chat Room Password");
        }  
    });
    
};

window.onload = function(){
    init();
};
